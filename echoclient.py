from twisted.internet.protocol import Protocol, ReconnectingClientFactory
from twisted.protocols.basic import LineReceiver
from twisted.internet import reactor
from sys import stdout, stdin

class Echo(LineReceiver):

    def connectionMade(self):
        self.transport.write(raw_input())

    def dataReceived(self, data):
        stdout.write(data)
        print
        self.transport.write(raw_input())

    def connectionLost(self, reason):
        print("Connection terminated")

class EchoClientFactory(ReconnectingClientFactory):
    def buildProtocol(self, addr):
        self.resetDelay()
        return Echo()

def main():
    # while True:

        reactor.connectTCP('127.0.0.1', 1234, EchoClientFactory())
        reactor.run()

main()
