from twisted.internet.protocol import Protocol, ServerFactory
from twisted.internet import reactor

class Echo(Protocol):

    def dataReceived(self, data):
        self.transport.write(data)

class EchoServerFactory(ServerFactory):
    def buildProtocol(self, addr):
        return Echo()

def main():
    reactor.listenTCP(1234, EchoServerFactory())
    reactor.run()

main()